# Backup_Site_Data
upon login, creates a timestamped targz of all my relevant site data+emails a link to it to the a user inputted email
Upon logout, removes that targz

I'll break each block down a little bit better, though if you're a novice in bash, it's all somewhat self-explanatory

So, for back.sh
clear
echo "Backups: created"
	^^The above is rather for my own aesthetic purposes ¯\_(ツ)_/¯
tar -zcvf "Scripts.$(date '+%Y-%m-%d').tar.gz" back.sh .bashrc  .bash_logout
	^^ The above is geared towards getting the actual scripts in one place, seperate from the HTML files. Organization is key
cp "Scripts.$(date '+%Y-%m-%d').tar.gz" public_html/"Scripts.$(date '+%Y-%m-%d').tar.gz"
	^^ Moves the newly created targz into public_html
cd public_html
	^^ Moves you into public_html
tar -zcvf "HTML.$(date '+%Y-%m-%d').tar.gz" *.html *.tar.gz
	^^ zips up all instances of HTML and that targz we made earlier
rm -rf bash*  back*
	^^ There was acouple of backup files and shell scripts sprayed all over a public_facing directory. You do that, and you're gonna have a bad time
cd
	^^^ Go home
	echo " Backup HTML.$(date '+%Y-%m-%d').tar.gz has been created! Navigate to site.com/HTML.$(date '+%Y-%m-%d').tar.gz ,  the file WILL be deleted once you log out" | mail -s "Backup Notification" youremail@email.com

	^^ Emails you your sweet new backup. Kinda like a quick n dirty 2FA, but for backups. Head on over to your email, and your backup will be there waiting for you. Once you log outta shell, your backup is gone! Plus, it serves a secondary purpose of notifying you whenever someone logs into the user.
Depending on your service provider, you can also text yourself directly, using similar instructions found here https://www.verizon.com/about/news/vzw/2013/06/computer-to-phone-text-messaging
